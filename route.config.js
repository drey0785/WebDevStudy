module.exports = {
    port: 1234,
    pages: {
        main: '/',
        button: '/button',
        dropdown: '/dropdown',
        input: '/input',
        modal: '/modal',
        popover: '/popover',
        table: '/table',
        textarea: '/textarea',
        calculator: '/calculator'
    }
};