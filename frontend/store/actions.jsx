import constants from './constants.js';

const setDispatch = (type, newState) => {
    return { type: type, newState: newState }
};

export function pressEquals(value) {
    return (dispatch) => {
        dispatch(setDispatch(constants.pressEquals, { force: true, value: null, operation: null }));
    }
}

export function pressReset() {
    return (dispatch) => {
        dispatch(setDispatch(constants.pressReset, { force: true, value: 0, operation: null }));
    }
}

export function pressDigit(value) {
    return (dispatch) => {
        dispatch(setDispatch(constants.pressDigit, { force: false, value: value, operation: null }));
    }
}

export function pressOperation(operation) {
    return (dispatch) => {
        dispatch(setDispatch(constants.pressOperation, { operation: operation, force: false, value: null }));
    }
}