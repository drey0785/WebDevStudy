import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'

import calculator from './reducer.jsx';

export default combineReducers({
    routing: routerReducer,
    calculator
});