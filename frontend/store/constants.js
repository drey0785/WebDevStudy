export default {
    pressEquals: 'CALCULATOR_PRESS_EQUALS',
    pressReset: 'CALCULATOR_PRESS_RESET',
    pressDigit: 'CALCULATOR_PRESS_DIGIT',
    pressOperation: 'CALCULATOR_PRESS_OPERATION',
};