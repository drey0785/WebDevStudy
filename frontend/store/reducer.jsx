import constants from './constants.js';

const initialState = {
    value: 0,
    text: "0",
    force: true,
    operation: null
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case constants.pressEquals:
            return {...state, ...action.newState };
        case constants.pressReset:
            return {...state, ...action.newState };
        case constants.pressDigit:
            return {...state, ...action.newState };
        case constants.pressOperation:
            return {...state, ...action.newState };
        default:
            return state;
    }
}