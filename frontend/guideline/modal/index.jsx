import React from 'react';
import ReactDOM from 'react-dom';

import './style.css';

export default class Modal extends React.Component {
    constructor(props) {
        super(props);

        this.closeBtn = React.createRef();

        this.changeState = this.changeState.bind(this);
        this.open = this.open.bind(this);

        this.state = {
            hidden: props.hidden !== false,
            //заголовок и контент можно менять в жизненном цикле окна
            title: props.title || "",
            content: props.content || ""
        };
    }

    open() {
        this.changeState(null, false);
    }

    componentWillUpdate() {
        if (this.state.hidden) {
            this.modalBackground = document.createElement('div');
            this.modalBackground.classList.add("modal");
            this.modalBackground.classList.add("background");
            this.modalBackground.onclick = function (e) {
                e.stopPropagation();
            }
            document.body.appendChild(this.modalBackground);
        } else {
            document.body.removeChild(this.modalBackground);
        }
    }

    changeState = (e, hidden) => {
        if (e) {
            e.stopPropagation();
        }
        this.setState({ hidden: hidden });
    }

    render() {
        return (
            <div className={"modal " + (this.state.hidden ? "hidden" : "")}>
                <div className={"title"}>
                    <div className={"content"}>{this.state.title}</div>
                    <div className={"close"} onClick={e => this.changeState(e, true)} ref={this.closeBtn}>×</div>
                </div>
                <div className={"content"}>{this.state.content}</div>
            </div>
        );
    }
}

/*
class ModalBackground extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={"modal background"}
                 onClick={(e) => {e.stopPropagation(); return false;}}>
            </div>
        );
    }
}*/