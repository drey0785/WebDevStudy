import React from 'react';
import ReactDOM from 'react-dom';

import './style.css';
import '../input/style.css';
import '../button/style.css';

export default class Calculator extends React.Component {
    constructor(props) {
        super(props);

        this.actions = this.props.actions;

        this.pressReset = this.pressReset.bind(this);
        this.pressEquals = this.pressEquals.bind(this);
        this.pressDigit = this.pressDigit.bind(this);
        this.pressOperation = this.pressOperation.bind(this);
    }

    pressReset = () => {
        this.actions.pressReset();
    }

    pressEquals = () => {
        this.actions.pressEquals(this.props.store.value);
    }

    pressDigit = digit => {
        this.actions.pressDigit(digit);
    }

    pressOperation = operation => {
        this.actions.pressOperation(operation);
    }

    /*change = e => {
        console.log(this.input.value);
    }*/

    render() {
        //console.log(this.props.store);
        let error = false;
        if (this.props.store.operation != null && !((this.props.store.text || "").endsWith(this.props.store.operation))) {
            this.props.store.text = this.props.store.text + this.props.store.operation;
        } else if (this.props.store.force) {
            try {
                this.props.store.text = this.props.store.value != null ? this.props.store.value : eval(this.props.store.text)
            } catch(e) {
                error = true;
            }
        } else if (this.props.store.value != null) {
            this.props.store.text = this.props.store.text != "0" ? this.props.store.text + this.props.store.value : this.props.store.value;
        }
        //явно в строку, чтобы операторы нормально отображались в input, а не выполняли математических действий следующий раз
        this.props.store.text = this.props.store.text + "";

        return (
            <div className={"calculator"}>
                <div>
                    <input placeholder={this.props.placeholder}
                           className={"input" + (error ? " error" : "")}
                           /*onchange={this.change}*/
                           value={this.props.store.text} />
                </div>
                <div>
                    <button className={"button"} onClick={() => {this.pressDigit(7);}}>7</button>
                    <button className={"button"} onClick={() => {this.pressDigit(8);}}>8</button>
                    <button className={"button"} onClick={() => {this.pressDigit(9);}}>9</button>
                    <button className={"button"} onClick={() => {this.pressOperation("/");}}>÷</button>
                </div>
                <div>
                    <button className={"button"} onClick={() => {this.pressDigit(4);}}>4</button>
                    <button className={"button"} onClick={() => {this.pressDigit(5);}}>5</button>
                    <button className={"button"} onClick={() => {this.pressDigit(6);}}>6</button>
                    <button className={"button"} onClick={() => {this.pressOperation("*");}}>×</button>
                </div>
                <div>
                    <button className={"button"} onClick={() => {this.pressDigit(1);}}>1</button>
                    <button className={"button"} onClick={() => {this.pressDigit(2);}}>2</button>
                    <button className={"button"} onClick={() => {this.pressDigit(3);}}>3</button>
                    <button className={"button"} onClick={() => {this.pressOperation("-");}}>-</button>
                </div>
                <div>
                    <button className={"button"} onClick={() => {this.pressReset();}}>C</button>
                    <button className={"button"} onClick={() => {this.pressDigit(0);}}>0</button>
                    <button className={"button"} onClick={() => {this.pressEquals();}}>=</button>
                    <button className={"button"} onClick={() => {this.pressOperation("+");}}>+</button>
                </div>
            </div>
        );
    }
}