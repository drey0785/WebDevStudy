import React from 'react';
import ReactDOM from 'react-dom';

import styles from './style.css';

export default class Popover extends React.Component {
    constructor(props) {
        super(props);

        this.changeClickState = this.changeClickState.bind(this);
        this.changeHoverState = this.changeHoverState.bind(this);

        this.state = {
            hidden: props.hidden !== false
        };
    }

    changeClickState(mode) {
        if ('click' != this.props.mode) {
            return;
        }
        this.setState({ hidden: !this.state.hidden });
    }

    changeHoverState(hidden) {
        if ('hover' != this.props.mode) {
            return;
        }
        this.setState({ hidden: hidden });
    }

    render() {
        return (
            <div>
                <span onClick={() => this.changeClickState('click', true)}
                      onMouseOver={() => this.changeHoverState(false)} onMouseOut={() => this.changeHoverState(true)}>
                    {this.props.title}
                </span>
                <div className={"popover " + (this.state.hidden ? "hidden" : "")}>
                    {this.props.content}
                </div>
            </div>
        );
    }
}

