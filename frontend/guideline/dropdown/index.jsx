import React from 'react';
import ReactDOM from 'react-dom';

import './style.css';
import '../input/style.css';

export default class DropDown extends React.Component {
    constructor(props) {
        super(props);

        this.input = React.createRef();

        this.changeState = this.changeState.bind(this);
        this.selectValue = this.selectValue.bind(this);

        this.state = {
            hidden: props.hidden !== false,
            value: props.value || ""
        };
    }


    componentDidMount() {
        this.setState({ width: this.input.current.clientWidth });

        //document.addEventListener('click', this.changeState);
    }

    componentWillUnmount() {
        //document.removeEventListener('click', this.changeState);
    }

    selectValue = e => {
        this.setState({ value: e.target.innerText });
        this.changeState(e);
    }

    changeState = e => {
        if (e) {
            e.stopPropagation();
        }
        this.setState({ hidden: !this.state.hidden });
    }

    render() {
        return (
            <div>
                <input placeholder={this.props.placeholder}
                       className={"input"}
                       onClick={this.changeState}
                       value={this.state.value}
                       ref={this.input}
                />
                <div className={"dropdown " + (this.state.hidden ? "hidden" : "")} style={{width: this.state.width + "px"}} >
                    {this.props.rows.map((row, i) => <div className={"item"} onClick={this.selectValue} key={i}>{row}</div>)}
                </div>
            </div>
        );
    }
}

