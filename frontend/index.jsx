﻿import './pages/page.css';
import './guideline/container/style.css';

import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configure-store.jsx'
import Router from './routing.jsx';

const store = configureStore({});

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Router />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);