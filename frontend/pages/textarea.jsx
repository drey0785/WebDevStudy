﻿import './page.css';
import '../guideline/textarea/style.css';

export default class Control extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div class="block">
                <h2>TextArea</h2>
                <p>
                    <textarea placeholder="Подсказка" class="textarea" rows="5">текст</textarea>
                </p>

                <h3>Example</h3>
                <code class="code">
                    &lt;textarea placeholder="Подсказка" class="textarea" rows="5"&gt;текст&lt;/textarea&gt;
                </code>

                <p>
                    <textarea placeholder="Подсказка" class="textarea" rows="5" disabled>текст</textarea>
                </p>

                <h3>Example</h3>
                <code class="code">
                    &lt;textarea placeholder="Подсказка" class="textarea" rows="5" disabled&gt;текст&lt;/textarea&gt;
                </code>
            </div>
        );
    }
}