﻿import './page.css';
import Popover from '../guideline/popover/index.jsx';

export default class Control extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div class="block">
                <h2>Popover</h2>

                <Popover mode="click"
                         title="Popover click"
                         content="This is content of popover. It's works on click action" />

                <h3>Example</h3>
                <code class="code">
                    &lt;Popover mode="click"<br/>
                    &nbsp;&nbsp;&nbsp;title="Popover click"<br/>
                    &nbsp;&nbsp;&nbsp;content="This is content of popover. It's works on click action" /&gt;
                </code>
                <br/>
                <Popover mode="hover"
                         title="Popover hover"
                         content="This is content of popover. It's works on hover action" />

                <h3>Example</h3>
                <code class="code">
                    &lt;Popover mode="hover"<br/>
                    &nbsp;&nbsp;&nbsp;title="Popover hover"<br/>
                    &nbsp;&nbsp;&nbsp;content="This is content of popover. It's works on hover action" /&gt;
                </code>
            </div>
        );
    }
}