﻿import './page.css';
import Calculator from '../guideline/calculator/index.jsx';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../store/actions.jsx'

class Control extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div class="block">
                <h2>Calculator</h2>
                <p>
                    <Calculator store={this.props.store} actions={this.props.actions}></Calculator>
                </p>

                <h3>Example</h3>
                <code class="code">
                    &lt;Calculator store={"{this.props.store}"} actions={"{this.props.actions}"}&gt;&lt;/Calculator&gt;
                </code>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        store: state.calculator,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Actions, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Control)