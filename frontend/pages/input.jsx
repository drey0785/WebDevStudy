﻿import './page.css';
import '../guideline/input/style.css';

export default class Control extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div class="block">
                <h2>Input</h2>
                <p>
                    <input placeholder="Подсказка" value="Текст" class="input" />
                </p>

                <h3>Example</h3>
                <code class="code">
                    &lt;input placeholder="Подсказка" value="Текст" class="input" /&gt;
                </code>

                <p>
                    <input placeholder="Подсказка" value="Текст" class="input" disabled />
                </p>

                <h3>Example</h3>
                <code class="code">
                    &lt;input placeholder="Подсказка" value="Текст" class="input" disabled /&gt;
                </code>
            </div>
        );
    }
}