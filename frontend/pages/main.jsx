﻿import './page.css';
import '../guideline/container/style.css';

export default class Control extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div class="block">
                <h2>Container</h2>

                <p>
                    <div class="container">
                        пример элемента контейнера
                    </div>
                </p>

                <h3>Example</h3>
                <code class="code">
                    &lt;div class="container"&gt;<br />
                    &nbsp;&nbsp;&nbsp;пример элемента контейнера<br />
                    &lt;/div&gt;<br />
                </code>
            </div>
        );
    }
}