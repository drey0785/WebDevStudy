﻿import './page.css';
import '../guideline/button/style.css';

export default class Control extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div class="block">
                <h2>Button</h2>
                <p>
                    <button class="button">button</button>
                </p>

                <h3>Example</h3>
                <code class="code">
                    &lt;button class="button"&gt;button&lt;/button&gt;
                </code>

                <p>
                    <button class="button" disabled>button</button>
                </p>

                <h3>Example</h3>
                <code class="code">
                    &lt;button class="button" disabled&gt;button&lt;/button&gt;
                </code>
            </div>
        );
    }
}