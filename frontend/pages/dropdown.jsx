﻿import './page.css';
import DropDown from '../guideline/dropdown/index.jsx';

export default class Control extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div class="block">
                <h2>Dropdown</h2>

                <DropDown
                    rows = {["Мандарин", "Апельсин", "Лимон"]}
                    placeholder="Выберите значение" />

                <h3>Example</h3>
                <code class="code">
                    &lt;DropDown<br/>
                    &nbsp;&nbsp;&nbsp;rows = {["Мандарин", "Апельсин", "Лимон"]}<br/>
                    &nbsp;&nbsp;&nbsp;placeholder="Выберите значение" /&gt;
                </code>
            </div>
        );
    }
}