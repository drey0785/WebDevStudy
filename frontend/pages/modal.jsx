﻿import './page.css';
import Modal from '../guideline/modal/index.jsx';

export default class Control extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.modal = React.createRef();
    }

    componentDidMount() {
        document.getElementById("openModalButton").onclick = this.modal.current.open;
    }

    render() {
        return (
            <div className={"block"}>
                <h2>Modal window</h2>

                <p>
                    <button id="openModalButton" className={"button"}>Open modal window</button>
                    <Modal title="Title of modal window"
                           content="This is content of modal window" ref={this.modal} />
                </p>

                <h3>Example</h3>
                <code className={"code"}>
                    var modal = ReactDOM.render(<br/>
                    &lt;Modal title="Title of modal window"<br/>
                    &nbsp;&nbsp;&nbsp;content="This is content of modal window" /&gt;, document.getElementById("container"));<br/>
                    <br/>
                    document.getElementById("openModalButton").onclick = modal.show;
                </code>
            </div>
        );
    }
}