﻿import './page.css';
import '../guideline/table/style.css';

export default class Control extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div class="block">
                <h2>Table</h2>
                <p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Продукты</th>
                            <th>Цена(руб.)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>1</th>
                            <td>Хлеб</td>
                            <td>35</td>
                        </tr>
                        <tr>
                            <th>2</th>
                            <td>Молоко</td>
                            <td>50</td>
                        </tr>
                        </tbody>
                    </table>
                </p>

                <h3>Example</h3>
                <code class="code">
                    &lt;table class="table"&gt;<br />
                    &nbsp;&nbsp;&nbsp;&lt;thead&gt;<br />
                    &nbsp;&nbsp;&nbsp;&lt;tr&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;th&gt;#&lt;/th&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;th&gt;Продукты&lt;/th&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;th&gt;Цена(руб.)&lt;/th&gt;<br />
                    &nbsp;&nbsp;&nbsp;&lt;/tr&gt;<br />
                    &nbsp;&nbsp;&nbsp;&lt;/thead&gt;<br />
                    &nbsp;&nbsp;&nbsp;&lt;tbody&gt;<br />
                    &nbsp;&nbsp;&nbsp;&lt;tr&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;th&gt;1&lt;/th&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td>Хлеб&lt;/td&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;35&lt;/td&gt;<br />
                    &nbsp;&nbsp;&nbsp;&lt;/tr&gt;<br />
                    &nbsp;&nbsp;&nbsp;&lt;tr&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;th&gt;2&lt;/th&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;Молоко&lt;/td&gt;<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;50&lt;/td&gt;<br />
                    &nbsp;&nbsp;&nbsp;&lt;/tr&gt;<br />
                    &nbsp;&nbsp;&nbsp;&lt;/tbody&gt;<br />
                    &lt;/table&gt;<br />
                </code>
            </div>
        );
    }
}