export default class Control extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.ul = React.createRef();
    };

    componentDidMount() {
        var menuLinks = this.ul.current.getElementsByTagName("a");
        for (var i = 0; i < menuLinks.length; i++) {
            var menuLink = menuLinks[i];
            if (menuLink.href == window.location.href) {
                menuLink.className = "active";
                break;
            }
        }
    }

    render() {
        return (
            <div className={"page-container"}>
                <header className={"header"}>
                    <ul ref={this.ul}>
                        <li><a href={routing.pages.main}>Container</a></li>
                        <li><a href={routing.pages.button}>Button</a></li>
                        <li><a href={routing.pages.input}>Input</a></li>
                        <li><a href={routing.pages.textarea}>TextArea</a></li>
                        <li><a href={routing.pages.table}>Table</a></li>
                        <li><a href={routing.pages.popover}>Popover</a></li>
                        <li><a href={routing.pages.dropdown}>Dropdown</a></li>
                        <li><a href={routing.pages.modal}>Modal window</a></li>
                        <li><a href={routing.pages.calculator}>Calculator</a></li>
                    </ul>
                </header>

                {this.props.children}
            </div>
        );
    }
}