import { Route, Switch } from 'react-router-dom';
import Layout from './pages/layout.jsx';
import Main from './pages/main.jsx';
import Button from './pages/button.jsx';
import Input from './pages/input.jsx';
import TextArea from './pages/textarea.jsx';
import Table from './pages/table.jsx';
import Popover from './pages/popover.jsx';
import DropDown from './pages/dropdown.jsx';
import Modal from './pages/modal.jsx';
import Calculator from './pages/calculator.jsx';
import NotFound from './pages/404.jsx';

const Router = () => (
    <Layout>
        <Switch>
            <Route exact path={routing.pages.main} component={Main}/>
            <Route exact path={routing.pages.button} component={Button}/>
            <Route exact path={routing.pages.input} component={Input}/>
            <Route exact path={routing.pages.textarea} component={TextArea}/>
            <Route exact path={routing.pages.table} component={Table}/>
            <Route exact path={routing.pages.popover} component={Popover}/>
            <Route exact path={routing.pages.dropdown} component={DropDown}/>
            <Route exact path={routing.pages.modal} component={Modal}/>
            <Route exact path={routing.pages.calculator} component={Calculator}/>
            <Route component={NotFound}/>
        </Switch>
    </Layout>
);

export default Router;