export default (props) => {
    return `<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width">

                <title>${props.title || "Web development training"}</title>
            </head>
            <body>
                <div id="root">
                </div>
            </body>
            </html>

            <script type="text/javascript" src="bundle.js"></script>
            `
}