import express from 'express';
import routing from '../route.config.js';
import Layout from './server-render';

const app = express();

app.use(routing.pages.main, express.static(__dirName + '/public'));
app.listen(routing.port);

const metaInfo = {
    [routing.pages.main]: 'main',
    [routing.pages.button]: 'button',
    [routing.pages.dropdown]: 'dropdown',
    [routing.pages.input]: 'input',
    [routing.pages.modal]: 'modal',
    [routing.pages.popover]: 'popover',
    [routing.pages.table]: 'table',
    [routing.pages.textarea]: 'textarea',
    [routing.pages.calculator]: 'calculator',
};

const pages =  Object.keys(metaInfo);

app.get(pages, (req, res) => {
    res.send(Layout({ title: metaInfo[req.path] }));
});

/*app.get('*', (req, res) => {
    res.send(Layout({ title: metaInfo[req.path] }));
});*/

console.log(`web server is running on port ${routing.port}`);